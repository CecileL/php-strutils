<?php

class StrUtils {
    private $str;
    public function __construct($str)
    {
        $this->str = $str;
    }
    public function bold() {
        return "<strong>" . $this->str . "</strong>";
    }
    public function italic() {
        return "<i>" . $this->str . "</i>";
    }
    public function underline() {
        return "<p style='text-decoration: underline;'>" . $this->str . "</p>";
    }
    public function capitalize() {
        return "<p style='text-transform: capitalize;'>" . $this->str . "</p>";
    }
    public function uglify() {
        $this->str = $this->bold();
        $this->str = $this->italic();
        $this->str = $this->underline();
        return $this->str;
    }
};

$mystr = new StrUtils('pouet');

echo $mystr->bold() . "\n";
echo $mystr->italic() . "\n";
echo $mystr->underline() . "\n";
echo $mystr->capitalize() . "\n";
echo $mystr->uglify();

?>